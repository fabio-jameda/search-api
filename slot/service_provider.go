package slot

import (
	"sync"

	"bitbucket.org/fabio-jameda/search-api/solr"
)

var solrLoaderOnce sync.Once
var solrLoader *SolrLoader

func GetSolrLoaderService() *SolrLoader {
	solrLoaderOnce.Do(func() {
		solrLoader = NewSolrLoader(solr.GetAdapterClientSlotService())
	})

	return solrLoader
}
