package slot

import (
	"context"

	"bitbucket.org/fabio-jameda/search-api/model"
	"bitbucket.org/fabio-jameda/search-api/solr"
)

type Loader interface {
	Load(ctx context.Context, params *model.Params) (*model.SearchResponse, error)
}

type SolrLoader struct {
	client solr.Client
}

func (l *SolrLoader) Load(ctx context.Context, params *model.Params) (*model.SearchResponse, error) {
	newParams := *params

	newParams.FilterRanges = []*model.FilterRange{
		{
			Field: "bookableFrom",
			From:  "*",
			To:    "NOW",
		},
		{
			Field: "bookableTo",
			From:  "NOW",
			To:    "*",
		},
		{
			Field: "slotDateTime",
			From:  "NOW",
			To:    "NOW+15DAYS",
		},
	}

	searchResult, err := l.client.Select(ctx, &newParams)

	if err != nil {
		return nil, err
	}

	return searchResult, nil
}

func NewSolrLoader(client solr.Client) *SolrLoader {
	return &SolrLoader{
		client: client,
	}
}
