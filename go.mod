module bitbucket.org/fabio-jameda/search-api

go 1.14

require (
	github.com/gorilla/mux v1.8.0
	github.com/joho/godotenv v1.3.0
	github.com/n0str3ss/logger v1.2.2
	github.com/vanng822/go-solr v0.10.0
)
