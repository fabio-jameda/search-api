# search-api

- docker-compose run --rm go go get ./... (intall dependency packages)
- docker-compose run --rm go go mod vendor (create vendor folder)
- docker-compose run --rm go go build main.go (build)
- ./main (run)
- in browser http://localhost:8001/slots

to test the docker image

- run first two steps from above
- docker-compose up -d --build api
- in browser http://localhost:8001/slots
