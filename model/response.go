package model

type Facet struct {
	Name  string `json:"name,omitempty"`
	Count int    `json:"count,omitempty"`
}

type Slot struct {
	Id                   string   `json:"id,omitempty"`
	RefId                string   `json:"refId,omitempty"`
	ServiceId            string   `json:"serviceId,omitempty"`
	BookableFrom         string   `json:"bookableFrom,omitempty"`
	BookableTo           string   `json:"bookableTo,omitempty"`
	SlotDate             string   `json:"slotDate,omitempty"`
	AvailabilityTimeTags []string `json:"availabilityTimeTags,omitempty"`
	WeekDay              string   `json:"weekDay,omitempty"`
	IsSelfPayer          bool     `json:"isSelfPayer"`
	IsOvs                bool     `json:"isOvs"`
	Insurance            string   `json:"insurance,omitempty"`
}

type SearchResponse struct {
	Total        int                    `json:"total"`
	Count        int                    `json:"count"`
	Offset       int                    `json:"offset"`
	Limit        int                    `json:"limit"`
	Items        []interface{}          `json:"items,omitempty"`
	Facets       map[string]interface{} `json:"facets,omitempty"`
	SolrResponse map[string]interface{} `json:"solrResponse,omitempty"`
}
