package model

import "strconv"

type Params struct {
	RefId           []string          `json:"refId,omitempty"`
	StandardService []string          `json:"standardService,omitempty"`
	Insurance       []string          `json:"insurance,omitempty"`
	AppointmentType []string          `json:"appointmentType,omitempty"`
	SolrFacetTree   []*SolrFacetTree  `json:"solrFacetTree,omitempty"`
	SolrFacetQuery  []*SolrFacetQuery `json:"solrFacetQuery,omitempty"`
	Limit           []string          `json:"limit,omitempty"`
	FilterRanges    []*FilterRange
}

type FilterRange struct {
	Field string
	From  string
	To    string
}

func (p *Params) IsOvs() (isOvs bool) {
	if len(p.AppointmentType) > 0 {
		if p.AppointmentType[0] == "video" {
			isOvs = true
		}
	}

	return isOvs
}

func (p *Params) GetInsurance() (insurance string) {
	givenInsurance := p.Insurance
	if len(givenInsurance) > 0 {
		if givenInsurance[0] == InsurancePrivate || givenInsurance[0] == InsuranceStatutory {
			insurance = givenInsurance[0]
		}
	}

	return insurance
}

func (p *Params) GetLimit() *int {
	var limit int

	if len(p.Limit) > 0 {
		limit, _ = strconv.Atoi(p.Limit[0])
	} else {
		limit = 30
	}

	return &limit
}
