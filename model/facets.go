package model

import (
	"strconv"
	"strings"
)

type SolrFacetTree string
type SolrFacetQuery string

const (
	InsurancePrivate   string = "private"
	InsuranceStatutory        = "statutory"
)

func (f *SolrFacetTree) GetKey() string {
	split := strings.Split(string(*f), "|")
	return split[0]
}

func (f *SolrFacetTree) GetFields() string {
	split := strings.Split(string(*f), "|")
	return split[1]
}

func (f *SolrFacetTree) GetMinCount() int {
	split := strings.Split(string(*f), "|")
	minCount, _ := strconv.Atoi(split[2])
	return minCount
}

func (f *SolrFacetQuery) GetGroupName() string {
	split := strings.Split(string(*f), "|")
	return split[0]
}

func (f *SolrFacetQuery) GetKey() string {
	split := strings.Split(string(*f), "|")
	return split[1]
}

func (f *SolrFacetQuery) GetQuery() string {
	split := strings.Split(string(*f), "|")
	return split[2]
}

func (f *SolrFacetQuery) GetExcludes() []string {
	split := strings.Split(string(*f), "|")
	return strings.Split(split[3], ",")
}
