package solr

import (
	"fmt"
	"strings"

	"bitbucket.org/fabio-jameda/search-api/model"
	"github.com/vanng822/go-solr/solr"
)

func buildQuery(q *solr.Query, p *model.Params) {
	q.Q("*:*")
	q.SetParam("facet", "true")
	buildRefIdQuery(q, p)
	buildStandardServiceQuery(q, p)
	buildSlotInsuranceQuery(q, p)
	buildSlotIsOvsQuery(q, p)
	buildFacetTreeQuery(q, p)
	buildPagination(q, p)
	buildRangeQuery(q, p)
}

func buildPagination(q *solr.Query, p *model.Params) {
	q.Start(0)

	limit := p.GetLimit()

	if limit != nil {
		q.Rows(*limit)
	}
}

func buildRefIdQuery(q *solr.Query, p *model.Params) {
	if len(p.RefId) > 0 {
		q.FilterQuery(fmt.Sprintf("refId: (%s)", strings.Join(p.RefId, " OR ")))
	}
}

func buildStandardServiceQuery(q *solr.Query, p *model.Params) {
	if len(p.StandardService) > 0 {
		q.FilterQuery(fmt.Sprintf("standardServiceName: (%s)", strings.Join(p.StandardService, " OR ")))
	}
}

func buildSlotInsuranceQuery(q *solr.Query, p *model.Params) {
	if p.GetInsurance() != "" {
		q.FilterQuery(fmt.Sprintf("insurance: *%s*", strings.ToUpper(p.GetInsurance())))
	}
}

func buildSlotIsOvsQuery(q *solr.Query, p *model.Params) {
	if p.IsOvs() {
		q.FilterQuery("isOvs: true")
	}
}

func buildFacetTreeQuery(q *solr.Query, p *model.Params) {
	if len(p.SolrFacetTree) > 0 {
		for _, f := range p.SolrFacetTree {
			q.AddFacetPivot(fmt.Sprintf("{!tag=%s}%s", f.GetKey(), f.GetFields()))
			q.SetFacetPivotMinCount(f.GetMinCount())
		}
	}
}

func buildRangeQuery(q *solr.Query, p *model.Params) {
	if len(p.FilterRanges) > 0 {
		for _, f := range p.FilterRanges {
			q.FilterQuery(fmt.Sprintf("{!tag=%s_range}%s :[%s TO %s]", f.Field, f.Field, f.From, f.To))
		}
	}
}
