package solr

import (
	"context"

	"bitbucket.org/fabio-jameda/search-api/model"
	"github.com/vanng822/go-solr/solr"
)

type Client interface {
	Select(ctx context.Context, params *model.Params) (*model.SearchResponse, error)
}

type SolrAdapterClient struct {
	client *solr.SolrInterface
}

func (c *SolrAdapterClient) Select(ctx context.Context, params *model.Params) (*model.SearchResponse, error) {
	solrQuery := solr.NewQuery()

	buildQuery(solrQuery, params)

	solrResponse := c.client.Search(solrQuery)
	solrResult, err := solrResponse.Result(nil)

	if err != nil {
		return nil, err
	}

	return mapResponse(solrResult, params), nil
}

func NewSolrAdapterClient(client *solr.SolrInterface) *SolrAdapterClient {
	return &SolrAdapterClient{
		client: client,
	}
}
