package solr

import (
	"encoding/json"

	"bitbucket.org/fabio-jameda/search-api/model"
)

func mapFacetPivot(f map[string]interface{}, p *model.Params) (mapped map[string]interface{}) {
	mapped = make(map[string]interface{})
	mappedFacetPivot := make(map[string]interface{})

	for key, facet := range f {
		for _, pivot := range facet.([]interface{}) {
			//transfrom into a struct to be easier to operate
			var p Pivot
			b, _ := json.Marshal(pivot.(map[string]interface{}))
			_ = json.Unmarshal(b, &p)

			mappedFacetPivot[p.Value] = mapPivotRecursive(p)
		}

		//find facet group name provided in params.SolrFacetTree
		for _, facetTree := range p.SolrFacetTree {
			if key == facetTree.GetFields() {
				mapped[facetTree.GetKey()] = mappedFacetPivot
			}
		}
	}

	return mapped
}

func mapPivotRecursive(p Pivot) (mapped interface{}) {
	if len(p.Pivot) > 0 {
		mapped = make(map[string]interface{})
		mappedNested := make(map[string]interface{})
		for _, nestedPivot := range p.Pivot {
			mappedNested[nestedPivot.Value] = mapPivotRecursive(nestedPivot)
		}
		mapped = mappedNested
	} else {
		mapped = p.Count
	}

	return mapped
}
