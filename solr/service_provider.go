package solr

import (
	"fmt"
	"os"
	"sync"
	"time"

	"github.com/vanng822/go-solr/solr"
)

var adapterClientOnce sync.Once
var adapterClient *SolrAdapterClient

func GetAdapterClientSlotService() *SolrAdapterClient {
	adapterClientOnce.Do(func() {
		solrHost := os.Getenv("SOLR_HOST")
		slotsCore := os.Getenv("SOLR_CORE_SLOTS")

		solrClient, err := solr.NewSolrInterface(solrHost, slotsCore)
		solrClient.SetBasicAuth("admin", "fhezrt274hrf")
		solrClient.SetTimeout(2 * time.Second)

		if err != nil {
			panic(fmt.Sprintf("not possible to connect to solr. Reason: %v", err.Error()))
		}

		adapterClient = NewSolrAdapterClient(solrClient)
	})

	return adapterClient
}
