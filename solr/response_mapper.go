package solr

import (
	"encoding/json"

	"bitbucket.org/fabio-jameda/search-api/model"
	"github.com/vanng822/go-solr/solr"
)

/**
* map slots in order to not expose internal fields from solr and to be able to operate on items
 */
func mapResponse(searchResult *solr.SolrResult, p *model.Params) (mapped *model.SearchResponse) {
	mapped = &model.SearchResponse{}
	if searchResult != nil {
		mapped.Total = searchResult.Results.NumFound
		mapped.Count = len(searchResult.Results.Docs)
		mapped.Offset = searchResult.Results.Start
		mapped.Limit = *p.GetLimit()
		mapped.Facets = make(map[string]interface{})

		var mappedSlots []interface{}

		for _, slot := range searchResult.Results.Docs {
			var mappedSlot model.Slot
			//marshal original slot and then unmarshal it into model.Slot
			msh, _ := json.Marshal(slot)
			_ = json.Unmarshal(msh, &mappedSlot)

			mappedSlots = append(mappedSlots, &mappedSlot)

		}

		mapped.Items = mappedSlots

		if facetPivot, found := searchResult.FacetCounts["facet_pivot"]; found {
			mapped.Facets = mapFacetPivot(facetPivot.(map[string]interface{}), p)
		}

	}

	return mapped
}
