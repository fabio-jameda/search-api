package solr

type Pivot struct {
	Count int     `json:"count,omitempty"`
	Value string  `json:"value,omitempty"`
	Pivot []Pivot `json:"pivot,omitempty"`
}
