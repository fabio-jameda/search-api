package http

import (
	"net/http"
)

func GetHealth(w http.ResponseWriter, r *http.Request) {
	respondOk(w, r, "OK")
	return
}
