package http

import "github.com/gorilla/mux"

type SlotsRouter struct {
	router *mux.Router
}

func (sr *SlotsRouter) SetRoutes() {
	sr.router.Methods("GET").Path("/slots").HandlerFunc(GetSlots)
}

func NewSlotsRouter(router *mux.Router) *SlotsRouter {
	return &SlotsRouter{
		router,
	}
}
