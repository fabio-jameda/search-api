package http

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/fabio-jameda/search-api/utils/error"
	"github.com/n0str3ss/logger"
)

func respondInternalServerError(w http.ResponseWriter, r *http.Request) {
	errorResponse := error.NewInternalServerErrorErrorResponse("Internal server error")
	responseBody := marshalErrorResponse(errorResponse)
	respond(w, http.StatusInternalServerError, responseBody)
}

func respondNotFound(w http.ResponseWriter, r *http.Request) {
	errorResponse := error.NewNotFoundErrorResponse("Not found")
	responseBody := marshalErrorResponse(errorResponse)
	respond(w, http.StatusNotFound, responseBody)
}

func respondNotImplemented(w http.ResponseWriter, r *http.Request) {
	errorResponse := error.NewNotImplementedErrorResponse("Not implemented")
	responseBody := marshalErrorResponse(errorResponse)
	respond(w, http.StatusNotImplemented, responseBody)
}

func respondBadRequest(w http.ResponseWriter, r *http.Request, errors []*error.Error) {
	errorResponse := error.NewBadRequestErrorResponse("Bad request", errors)
	responseBody := marshalErrorResponse(errorResponse)
	respond(w, http.StatusBadRequest, responseBody)
}

func respondOk(w http.ResponseWriter, r *http.Request, resource interface{}) {
	responseBody, _ := json.Marshal(resource)
	respond(w, http.StatusOK, responseBody)
}

func respond(w http.ResponseWriter, statusCode int, responseBody []byte) {
	setHttpHeaders(w, statusCode)
	_, err := w.Write(responseBody)

	if err != nil {
		logger.LogErr(err)
	}
}

func setHttpHeaders(w http.ResponseWriter, statusCode int) {
	w.Header().Set("Content-Type", "application/json; charset=UTF-8")
	w.WriteHeader(statusCode)
}

func marshalErrorResponse(errors *error.ErrorResponse) []byte {
	body, err := json.Marshal(errors)

	if err != nil {
		logger.LogErr(err)
		return nil
	}

	return body
}
