package http

import "github.com/gorilla/mux"

type HealthRouter struct {
	router *mux.Router
}

func (hr *HealthRouter) SetRoutes() {
	hr.router.Methods("GET").Path("/health/search").HandlerFunc(GetHealth)
}

func NewHealthRouter(router *mux.Router) *HealthRouter {
	return &HealthRouter{
		router,
	}
}
