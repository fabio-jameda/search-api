package http

import (
	"net/http"
	"os"
	"sync"

	"github.com/gorilla/mux"
)

var serverOnce sync.Once
var server *http.Server
var routerOnce sync.Once
var router *mux.Router
var slotsRouterOnce sync.Once
var slotsRouter *SlotsRouter
var healthRouterOnce sync.Once
var healthRouter *HealthRouter

func GetServerService() *http.Server {
	serverOnce.Do(func() {
		listenOn := os.Getenv("LISTEN")
		server = NewServer(GetRouterService(), listenOn)
	})

	return server
}

func GetRouterService() *mux.Router {
	routerOnce.Do(func() {
		router = mux.NewRouter()
		router.NotFoundHandler = http.HandlerFunc(HandleNotFound)
	})
	return router
}

func GetSlotsRouterService() *SlotsRouter {
	slotsRouterOnce.Do(func() {
		slotsRouter = NewSlotsRouter(GetRouterService())
	})
	return slotsRouter
}

func GetHealthRouterService() *HealthRouter {
	healthRouterOnce.Do(func() {
		healthRouter = NewHealthRouter(GetRouterService())
	})
	return healthRouter
}
