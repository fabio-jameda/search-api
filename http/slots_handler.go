package http

import (
	"encoding/json"
	"net/http"

	"bitbucket.org/fabio-jameda/search-api/model"
	"bitbucket.org/fabio-jameda/search-api/slot"
	"github.com/n0str3ss/logger"
)

func GetSlots(w http.ResponseWriter, r *http.Request) {
	loader := slot.GetSolrLoaderService()
	var params model.Params

	b, _ := json.Marshal(r.URL.Query())
	_ = json.Unmarshal(b, &params)
	resp, err := loader.Load(r.Context(), &params)

	if err != nil {
		logger.LogErr(err)
		respondInternalServerError(w, r)
	} else {
		respondOk(w, r, resp)
	}

	return
}
