package http

import "net/http"

func HandleNotFound(w http.ResponseWriter, _ *http.Request) {
	respondNotFound(w, nil)
	return
}

func HandleNotImplemented(w http.ResponseWriter, _ *http.Request) {
	respondNotImplemented(w, nil)
	return
}
