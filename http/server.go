package http

import (
	"net/http"
	"time"

	"github.com/gorilla/mux"
)

func NewServer(router *mux.Router, portToListen string) *http.Server {
	return &http.Server{
		Addr:         portToListen,
		Handler:      router,
		WriteTimeout: time.Duration(3) * time.Second,
		ReadTimeout:  time.Duration(3) * time.Second,
	}
}
