package error

import "net/http"

type Error struct {
	Location     string `json:"location"`
	LocationType string `json:"locationType"`
	Reason       string `json:"reason"`
	Message      string `json:"message,omitempty"`
}

type ErrorResponse struct {
	Code    int      `json:"code"`
	Reason  string   `json:"reason"`
	Message string   `json:"message,omitempty"`
	Errors  []*Error `json:"errors,omitempty"`
}

func (r *ErrorResponse) Error() string {
	return r.Message
}

func NewError(location string, locationType string, reason string, message string) *Error {
	return &Error{
		Location:     location,
		LocationType: locationType,
		Reason:       reason,
		Message:      message,
	}
}

func NewErrorResponse(statusCode int, reason string, message string, errors []*Error) *ErrorResponse {
	return &ErrorResponse{
		Code:    statusCode,
		Reason:  reason,
		Message: message,
		Errors:  errors,
	}
}

func NewNotImplementedErrorResponse(message string) *ErrorResponse {
	status := http.StatusNotImplemented
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}

func NewNotFoundErrorResponse(message string) *ErrorResponse {
	status := http.StatusNotFound
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}

func NewBadRequestErrorResponse(message string, errors []*Error) *ErrorResponse {
	status := http.StatusBadRequest
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		errors,
	)
}

func NewUnauthorizedErrorResponse(message string) *ErrorResponse {
	status := http.StatusUnauthorized
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}

func NewAccessDeniedErrorResponse(message string) *ErrorResponse {
	status := http.StatusForbidden
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}

func NewInternalServerErrorErrorResponse(message string) *ErrorResponse {
	status := http.StatusInternalServerError
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}

func NewServiceUnavailableErrorResponse(message string) *ErrorResponse {
	status := http.StatusServiceUnavailable
	return NewErrorResponse(
		status,
		http.StatusText(status),
		message,
		nil,
	)
}
