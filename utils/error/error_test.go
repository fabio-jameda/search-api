package error_test

import (
	"testing"

	"bitbucket.org/fabio-jameda/search-api/utils/error"
)

func TestNewError(t *testing.T) {
	expectedLocation := "location"
	expectedLocationType := "location type"
	expectedReason := "reason"
	expectedMessage := "message"

	errorResult := error.NewError(expectedLocation, expectedLocationType, expectedReason, expectedMessage)

	if errorResult.Location != expectedLocation {
		t.Errorf("wrong location. Expected %v, got %v", expectedLocation, errorResult.Location)
	}

	if errorResult.LocationType != expectedLocationType {
		t.Errorf("wrong location. Expected %v, got %v", expectedLocationType, errorResult.LocationType)
	}

	if errorResult.Reason != expectedReason {
		t.Errorf("wrong location. Expected %v, got %v", expectedReason, errorResult.Reason)
	}

	if errorResult.Message != expectedMessage {
		t.Errorf("wrong location. Expected %v, got %v", expectedMessage, errorResult.Message)
	}
}
