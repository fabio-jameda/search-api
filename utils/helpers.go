package utils

import "runtime"

// returns true if strings slice contains given string
func IsStringInSlice(a string, list []string) bool {
	for _, b := range list {
		if b == a {
			return true
		}
	}
	return false
}

// returns frame with runtime info about file, line and method of where the function executes
func TraceFrame(skip int) runtime.Frame {
	pc := make([]uintptr, 15)
	n := runtime.Callers(skip, pc)
	frames := runtime.CallersFrames(pc[:n])
	frame, _ := frames.Next()
	return frame
}

// returns function name inside which the below function was called
func GetFunctionName() string {
	return TraceFrame(3).Function
}
