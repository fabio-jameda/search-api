package main

import (
	"fmt"

	myHttp "bitbucket.org/fabio-jameda/search-api/http"
	_ "github.com/joho/godotenv/autoload"
)

func main() {
	setup()
	server := myHttp.GetServerService()
	var err error

	fmt.Printf("Server started, listen on address %s \n", server.Addr)
	err = server.ListenAndServe()

	if err != nil {
		fmt.Println(err)
	}
}

func setup() {
	myHttp.GetSlotsRouterService().SetRoutes()
	myHttp.GetHealthRouterService().SetRoutes()
}
